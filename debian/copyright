Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: KronaTools
Source: https://github.com/marbl/Krona/releases
Files-Excluded: */ExcelTemplate
                */tar.sh
Comment: The package was rejected with the original name kronatools
    https://lists.alioth.debian.org/pipermail/debian-med-packaging/2016-December/049111.html
  As discussed in the ITP bug
    https://bugs.debian.org/832613
  upstream suggests the name "radiant" to avoid trademark conflicts
  (see below)

Files: *
Copyright: 2011-2016 Battelle National Biodefense Institute (BNBI)
                Brian Ondov, Nicholas Bergman, and Adam Phillippy
License: BSD-3-clause
Comment: TRADEMARK LICENSE
 KRONA™ is a trademark of the Department of Homeland Security, and use
 of the trademark is subject to the following conditions:
 .
  * Distribution of the unchanged, official code/software using the
    KRONA™ mark is hereby permitted by the Department of Homeland
    Security, provided that the software is distributed without charge
    and modification.
  * Distribution of altered source code/software using the KRONA™ mark
    is not permitted unless written permission has been granted by the
    Department of Homeland Security.
 .
 As discussed with upstream (https://github.com/marbl/Krona/issues/16)
 the trademark for KRONA™ does not affect the name kronatools.

Files: debian/*
Copyright: 2016-2022 Andreas Tille <tille@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 This Software was prepared for the Department of Homeland Security
 (DHS) by the Battelle National Biodefense Institute, LLC (BNBI) as
 part of contract HSHQDC-07-C-00020 to manage and operate the National
 Biodefense Analysis and Countermeasures Center (NBACC), a Federally
 Funded Research and Development Center.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
  * Neither the name of the Battelle National Biodefense Institute nor
    the names of its contributors may be used to endorse or promote
    products derived from this software without specific prior written
    permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
